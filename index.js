const express = require(`express`);
const app = express();
const port = 3020;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// solution #1
// process a GET request at the /home route that will print out a simple message
app.get("/home", (req, res) => {
    res.send(`Welcome to the home page!`)
})

// solution #3
// Create a GET route that will access the /users route that will retrieve all the users in the mock database
const user = [
    {
        userName: "mikhaambrosio",
        password: "kyla2137293"
    },
    {
        userName: "jasonpatacsil",
        password: "oliviarose26"
    }
];
app.get("/users", (req, res) => {
    let database = JSON.stringify(user)
    res.send(`${database}`)
})

// solution #5
// create a DELETE route that will access the /delete-user route to remove a user from the mock database

app.delete("/delete-user", (req, res) => {
    const {userName} = req.body
    let message;
    for (let i=0; i< user.length; i++){
        if(user[i].userName === userName){
            message = `User ${userName}'s has been deleted from the database`
            console.log(user)
            break
        } else {
            message = `User does not exist.`
        }
    }
    
    res.status(200).send(message)
})



// Listen port
app.listen(port, () => console.log(`Server is connected to port ${port}`));